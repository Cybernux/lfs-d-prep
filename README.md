# LFS D Prep

*LFS D Prep* is short for *Linux From Scratch and Debian Host Preparation*. This bash script will prepare a Debian host for building LFS.